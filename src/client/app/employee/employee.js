(function () {
    'use strict';

    angular
        .module('app.employee') //module defines an area of functionality in your app
        .controller('Employee', Employee); //1st param: name of the controler. 2nd param: function where we can find the controller

    Employee.$inject = ['logger', 'dataservice']; //load dependancies; inject dependancies

    /* @ngInject */
    function Employee(logger, dataservice) { //notice the dependancy injections being passed as parameters for use inside the function
        /*jshint validthis: true */
        var self = this; //reference the controller by assigning |this| to the variable |self|

        self.title = 'Employee'; //set controller property |title| to |'Admin'|. By doing this, you have access to this variable in the view |admin.html|
        self.people = [];

        activate();

        function activate() {
            logger.info('Activated Employee View');
            dataservice.getPeople().then(function(data){
                self.people = data;
            });
        }
        
    }
})();