(function () {
    'use strict';

    angular
        .module('app.employee')
        .run(appRun);

    appRun.$inject = ['routehelper'];

    /* @ngInject */
    function appRun (routehelper){
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return [
            {
                url: '/employee',
                config: {
                    title: 'employee',
                    controller: 'Employee', //this is the controller
                    controllerAs: 'vm',  //this will be the cotroller's allias to use in the view |admin.html|
                    templateUrl: 'app/employee/employee.html',
                    settings: {
                        nav: 2,
                        content: '<i class="fa fa-user"></i> Employee'
                    }
                }
            }
        ];
    }
})();