(function () {
    'use strict';

    angular
        .module('app.manager')
        .run(appRun);

    appRun.$inject = ['routehelper'];

    /* @ngInject */
    function appRun (routehelper){
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return [
            {
                url: '/manager',
                config: {
                    title: 'manager',
                    controller: 'Manager', //this is the controller
                    controllerAs: 'vm',  //this will be the cotroller's allias to use in the view |admin.html|
                    templateUrl: 'app/manager/manager.html',
                    settings: {
                        nav: 3,
                        content: '<i class="fa fa-users"></i> Manager'
                    }
                }
            }
        ];
    }
})();