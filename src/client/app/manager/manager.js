(function () {
    'use strict';

    angular
        .module('app.manager') //module defines an area of functionality in your app
        .controller('Manager', Manager); //1st param: name of the controler. 2nd param: function where we can find the controller

    Manager.$inject = ['logger', 'dataservice']; //load dependancies; inject dependancies

    /* @ngInject */
    function Manager(logger, dataservice) { //notice the dependancy injections being passed as parameters for use inside the function
        /*jshint validthis: true */
        var self = this; //reference the controller by assigning |this| to the variable |self|

        self.title = 'Manager'; //set controller property |title| to |'Admin'|. By doing this, you have access to this variable in the view |admin.html|
        self.people = [];

        activate();

        function activate() {
            logger.info('Activated Manager View');
            dataservice.getPeople().then(function(data){
                self.people = data;
            });
        }
        
        var data = [
            ["", "", "", ""],
            ["", null, null, null],
            ["", null, null, null],
            ["", null, null, null],
            ["", null, null, null],
            ["", null, null, null],
            ["", null, null, null],
            ["", null, null, null],
            ["", null, null, null],
            ["", null, null, null]
          ];
        
          var container = document.getElementById('hot');
          var hot = new Handsontable(container,
            {
              data: data,
              minSpareRows: 1,
              startRows: 5,
              startCols: 5,
              colHeaders: false,
              contextMenu: false
            });

        
    }
    
})();