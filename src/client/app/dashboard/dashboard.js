(function () {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('Dashboard', Dashboard);

    Dashboard.$inject = ['$q', 'dataservice', 'logger'];

    /* @ngInject */
    function Dashboard($q, dataservice, logger) {
        /*jshint validthis: true */
        var vm = this;


        //********View variables**************
        vm.news = {
            title: 'Six Sigma Grpah',
            description: 'Put graph and data here.'
        };
        vm.messageCount = 0;
        vm.people = [];
        vm.title = 'Dashboard';
        vm.toggleHidden = true;
        vm.allowCollapse = true;
        vm.line_data = [
          {x: 0, value: 4, otherValue: 14},
          {x: 1, value: 8, otherValue: 1},
          {x: 2, value: 15, otherValue: 11},
          {x: 3, value: 16, otherValue: 147},
          {x: 4, value: 23, otherValue: 87},
          {x: 5, value: 42, otherValue: 45}
        ];
        vm.line_options = {
          axes: {
            x: {key: 'x', labelFunction: function(value) {return value;}, type: 'linear', min: 0, max: 5, ticks: 2},
            y: {type: 'linear', min: 0, max: 147, ticks: 5},
            y2: {type: 'linear', min: 0, max: 147, ticks: [1, 2, 3, 4]}
          },
          series: [
            {y: 'value', color: 'steelblue', thickness: '2px', type: 'area', striped: true, label: 'Pouet'},
            {y: 'otherValue', axis: 'y2', color: 'lightsteelblue', visible: false, drawDots: true, dotSize: 2}
          ],
          lineMode: 'linear',
          tension: 0.7,
          tooltip: {mode: 'scrubber', formatter: function(x, y, series) {return 'pouet';}},
          drawLegend: true,
          drawDots: true,
          columnsHGap: 5
        }
        vm.bar_data = [
          {x: 0, val_0: 0, val_1: 0, val_2: 0, val_3: 0},
          {x: 1, val_0: 0.993, val_1: 3.894, val_2: 8.47, val_3: 14.347},
          {x: 2, val_0: 1.947, val_1: 7.174, val_2: 13.981, val_3: 19.991},
          {x: 3, val_0: 2.823, val_1: 9.32, val_2: 14.608, val_3: 13.509},
          {x: 4, val_0: 3.587, val_1: 9.996, val_2: 10.132, val_3: -1.167},
          {x: 5, val_0: 4.207, val_1: 9.093, val_2: 2.117, val_3: -15.136},
          {x: 6, val_0: 4.66, val_1: 6.755, val_2: -6.638, val_3: -19.923},
          {x: 7, val_0: 4.927, val_1: 3.35, val_2: -13.074, val_3: -12.625},
          {x: 8, val_0: 4.998, val_1: -0.584, val_2: -14.942, val_3: 2.331},
          {x: 9, val_0: 4.869, val_1: -4.425, val_2: -11.591, val_3: 15.873},
          {x: 10, val_0: 4.546, val_1: -7.568, val_2: -4.191, val_3: 19.787},
          {x: 11, val_0: 4.042, val_1: -9.516, val_2: 4.673, val_3: 11.698},
          {x: 12, val_0: 3.377, val_1: -9.962, val_2: 11.905, val_3: -3.487},
          {x: 13, val_0: 2.578, val_1: -8.835, val_2: 14.978, val_3: -16.557},
          {x: 14, val_0: 1.675, val_1: -6.313, val_2: 12.819, val_3: -19.584}
        ];
        vm.bar_options = {
          series: [
            {
              y: "val_0",
              label: "A colorful area series",
              color: "#ff7f0e",
              type: "area",
              thickness: "1px"
            }
          ],
          axes: {x: {type: "linear", key: "x"}, y: {type: "linear"}},
          lineMode: "linear",
          tension: 0.7,
          tooltip: {mode: 'axes', interpolate: false}
        };
        
        
        //***********end of variables***************

        activate();

        function activate() {
            var promises = [getMessageCount(), getPeople()];
            return $q.all(promises).then(function(){
                logger.info('Activated Dashboard View');
            });
        }

        function getMessageCount() {
            return dataservice.getMessageCount().then(function (data) {
                vm.messageCount = data;
                return vm.messageCount;
            });
        }

        function getPeople() {
            return dataservice.getPeople().then(function (data) {
                vm.people = data;
                return vm.people;
            });
        }

    }
})();
